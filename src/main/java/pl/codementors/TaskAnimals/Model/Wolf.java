package pl.codementors.TaskAnimals.Model;

public class Wolf extends Animal {
    public Wolf() {
    }

    public Wolf(String name, int age) {
        super(name, age);
    }

    public void awu() {
        System.out.println("awuuuuuuuuuuuuu");
    }
}
