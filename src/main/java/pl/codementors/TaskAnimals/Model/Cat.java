package pl.codementors.TaskAnimals.Model;

public class Cat extends Animal {

    public Cat(String name, int age) {
        super(name, age);
    }

    public void miau () {
        System.out.println("miauu");
    }
}
