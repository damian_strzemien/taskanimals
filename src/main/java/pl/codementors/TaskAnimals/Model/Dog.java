package pl.codementors.TaskAnimals.Model;

public class Dog extends Animal {
    public Dog(String name, int age) {
        super(name, age);
    }

    public void hau () {

        System.out.println("Hauuu Hauuu");
    }
}
