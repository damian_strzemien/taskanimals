package pl.codementors.TaskAnimals;


import pl.codementors.TaskAnimals.Model.Animal;
import pl.codementors.TaskAnimals.Model.Cat;
import pl.codementors.TaskAnimals.Model.Dog;
import pl.codementors.TaskAnimals.Model.Wolf;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        Animal[] arrAnimals = new Animal[5];

        //Poliformizm
        Animal jakies = null;



        boolean running = true;
        System.out.println("catalog of animals: ");

        while (running) {
            System.out.println("Podaj numer ");
            System.out.println("1 - Dodaj nowe zwierzę");
            System.out.println("2 - Wypisz wybrane zwierzę ");
            System.out.println("3 - Wypisz wszystkie zwierzęta ");
            System.out.println("4 - Co mówią zwierzęta");
            System.out.println("5 - wyjście");
            int command = scanner.nextInt();

            switch (command) {

                case 1: {
                    System.out.println("Jaki typ zwierzecia: C - cat, D - dog, W- wolf");
                    String kindAnimal = scanner.next();


                    System.out.println("Name");
                    String name = scanner.next();
                    System.out.println("age");
                    int age = scanner.nextInt();
                    System.out.println("Do której klatki dać zwierzę?");
                    int nrCage = scanner.nextInt();

                    Animal animal = null;
                    if ("W".equalsIgnoreCase(kindAnimal)) {
                       animal = new Wolf(name, age);
//                       animal.setName(name);
//                       animal.setAge(age);
                    }

                    if ("C".equalsIgnoreCase(kindAnimal)) {
                        animal = new Cat(name, age);
                    }

                    if ("D".equalsIgnoreCase(kindAnimal)) {
                        animal = new Dog(name, age);
                    }


                    System.out.println("created animal ");
                    System.out.println("name " + animal.getName());
                    System.out.println("age " + animal.getAge());

                    arrAnimals[nrCage] = animal;
                    System.out.println(nrCage);

                    break;
                }

                case 2: {
                    System.out.println("Z której klatki pokazać zwierzaka");
                    int nrCage = scanner.nextInt();
                    System.out.println(arrAnimals[nrCage].getName());
                    break;

                }

                case 3: {
                    for (Animal animal : arrAnimals) {
                        if (animal != null) {
                            System.out.println("Imię " + animal.getName() + " wiek " + animal.getAge());
                        }
                    }
                    break;
                }

                case 4: {

                    System.out.println("wpisz słowo voice");
                    String voice = scanner.next();

                    if ("voice".equals(voice)) {

                        for (Animal animal : arrAnimals) {
                            if (animal instanceof Wolf) {
                                Wolf wolf = (Wolf) animal;
                                wolf.awu();
                            }
                            else if (animal instanceof Dog) {
                                ((Dog)animal).hau();
                            }
                            else if (animal instanceof Cat) {
                                ((Cat)animal).miau();
                            }
                        }
                        break;
                    }
                }

                default:
                    System.out.println( "Wcisnąłeś zły klawisz");
                    running = false;
                    break;
            }


        }
    }
}